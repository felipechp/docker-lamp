composer update symfony/flex

composer require --dev \
    symfony/profiler-pack  \
    symfony/maker-bundle \
    sec-checker 

composer require \
    symfony/apache-pack \
    annotations \
    symfony/twig-bundle \
    asset \
    symfony/orm-pack \
    symfony/requirements-checker
